'use strict';

var JSZip = require('jszip');
var Docxtemplater = require('docxtemplater');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
var firebase = require("firebase-admin"); /*firebase admin sdk*/
var serviceAccount = require("./firebase/***************");
firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: "*********/",
});

exports.GenerateOptingOutGerantGet = async function (req, res) //recuperation de data sur la personne via un code
{
    var code = req.params.id;
    console.log("****Begin Generate Doc Status For Client***********" + code);
    GenerateOptingOutGerant(req, res, code);
};

async function GenerateOptingOutGerant(req, res, code) {

        var json = {'sName': '','adress':''};
        var template = "DOC/Sàrl-Opting-out-gérant.docx";

        json = await GetJsonFromFirebase(json, code);

        var dataDOC = await GenerateWordwithImg(template, json, 150, 50);

        var dataPdf = await ConvertDOcVerPdf(dataDOC, code);
        //delete file
        const path1 = 'C:\\' + code + '.docx';
        const path2 = 'C:\\' + code + '.pdf';
        try {
            fs.unlinkSync(path1)
            fs.unlinkSync(path2)
            //file removed
        } catch (err) {
            console.error(err)
        }

    res.writeHead(200, {
        'Content-Type': 'application/pdf',
        'Content-Disposition': 'attachment; filename=Opting-out-gerant.pdf',
        'Content-Length': dataPdf.length
    });
    res.status(200);
    console.log("****End Generate Doc ****");
    res.end(dataPdf);

}

function GetJsonFromFirebase(json, code) {
    return new Promise(resolve => {
        JSON.stringify(json);
        var db = firebase.database();

        var societeRef = db.ref("society");
        societeRef.child(code).once("value", function (snapshot) {
            var data = snapshot.val(); //Data is in JSON format.
            json.sName = data.sName;
            json.adress = data.adress;
            json.Datejour = moment(data.created).format("DD");
            json.Datemois = moment(data.created).format("MM");
            json.DateAnne = moment(data.created).format("YYYY");
            resolve(json);
        });
    });
}

function GenerateWordwithImg(template, DataInput, longueur, largeur) {
    //Load the docx file as a binary
    var content = fs.readFileSync(path.resolve(__dirname, template), 'binary');
    var ImageModule = require('docxtemplater-image-module');

    const imageOpts = {
        getImage(tag) {
            return base64DataURLToArrayBuffer(tag);
        },
        getSize(img, tagValue, tagName) {
            if (tagName === "signatureUrl")
                return [120, 50];
            else if (tagName === "qrcode")
                return [125, 125];
            else
                return [longueur, largeur]
        }
    };

    var imageModule = new ImageModule(imageOpts);
    var zip = new JSZip(content);
    var doc = new Docxtemplater();
    doc.loadZip(zip);
    doc.setData(DataInput);
    doc.attachModule(imageModule);

    try {
        doc.render()
    } catch (error) {
        var e = {
            message: error.message,
            name: error.name,
            stack: error.stack,
            properties: error.properties,
        };
        console.log(JSON.stringify({error: e}));
        // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
        throw error;
    }

    return  doc.getZip().generate({type: 'nodebuffer'});

}

function ConvertDOcVerPdf(json, code) {

    return new Promise((resolve, reject) => {
        fs.writeFileSync('C:\\' + code + '.docx', json);
        resolve(code + '.docx');
    })

        .then((file) => {

            return new Promise((resolve, reject) => {
                const word2pdf = require('word2pdf');
                word2pdf('C:\\' + file).then(data => {
                    fs.writeFileSync('C:\\' + code + '.pdf', data);
                    resolve(code + ".pdf");

                }).catch(err => {
                    console.log(err)
                });
            })
        })
        .then((file) => {
            return new Promise((resolve, reject) => {
                var content2 = fs.readFileSync('C:\\' + file);
                var buf = new Buffer(content2);
                resolve(buf);
            })
        });
}

/*https://docxtemplater.com/modules/image/#base64include*/
function base64DataURLToArrayBuffer(dataURL) {
    const base64Regex = /^data:image\/(png|jpg|svg|svg\+xml);base64,/;
    if (!base64Regex.test(dataURL)) {
        return false;
    }
    const stringBase64 = dataURL.replace(base64Regex, "");
    let binaryString;
    if (typeof window !== "undefined") {
        binaryString = window.atob(stringBase64);
    } else {
        binaryString = new Buffer(stringBase64, "base64").toString("binary");
    }
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
        const ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes.buffer;
}